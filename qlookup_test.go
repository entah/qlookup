package qlookup

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type (
	Person struct {
		Name             string
		Age              int
		Friends          []Person
		Address          Address
		Pets             []map[string]interface{}
		Hobbies          []string
		Todos            map[string]string
		YearlyActivities map[int]map[int]string
	}

	Address struct {
		City     string
		Province string
	}

	PetMenusFeed struct {
		Morning   string
		Afternoon string
		Evening   string
		Night     string
	}

	PetIdentity struct {
		Name   string
		Origin string
		Age    int
	}
)

var (
	fuja = Person{
		Name: "Fuja",
		Age:  21,
		Address: Address{
			City:     "Bogor",
			Province: "Jawa Barat",
		},
		Pets: []map[string]interface{}{
			{
				"kind":  "cat",
				"total": 2,
				"petMenusFeed": PetMenusFeed{
					Morning:   "fried fish",
					Afternoon: "fried chicken",
					Night:     "beef burger",
				},
				"indentities": []PetIdentity{
					{
						Name:   "Gar",
						Origin: "unknown",
					},
					{
						Name:   "Field",
						Origin: "unknown",
						Age:    10,
					},
				},
			},
			{
				"kind":  "mouse",
				"total": 5,
			},
		},
	}

	hadi = Person{
		Name: "Hadi",
		Age:  20,
		Address: Address{
			City:     "Bandung",
			Province: "Jawa Barat",
		},
		Friends: []Person{fuja},
		Hobbies: []string{"grumbling", "watching", "sleeping"},
		YearlyActivities: map[int]map[int]string{
			2019: {11: "Back to Bandung"},
			2010: {
				03: "Resign from Z*****",
				04: "Start a new life",
			},
		},
	}

	alfan = Person{
		Name: "Alfan",
		Age:  20,
		Address: Address{
			City:     "Jakarta",
			Province: "DKI Jakarta",
		},
		Friends: []Person{fuja, hadi},
		Todos: map[string]string{
			"eat":   "done",
			"sleep": "done",
			"poop":  "on progress",
		},
	}

	peoples = []Person{alfan, hadi, fuja}
)

func TestLookup(t *testing.T) {
	type args struct {
		lookup interface{}
		tree   interface{}
	}
	tests := []struct {
		name string
		args args
		want interface{}
	}{
		{
			"Key Found",
			args{
				lookup: Fields{"Name"},
				tree:   alfan,
			},
			struct{ Name string }{"Alfan"},
		},
		{
			"Key Found on level 2 struct",
			args{
				lookup: Fields{Pair{"Address": Fields{"City"}}},
				tree:   alfan,
			},
			struct {
				Address struct {
					City string
				}
			}(struct {
				Address struct {
					City string
				}
			}{Address: struct{ City string }{City: "Jakarta"}}),
		},
		{
			"Key Not Found",
			args{
				lookup: Fields{"Parents"},
				tree:   alfan,
			},
			struct{}{},
		},
		{
			"Pair Elem",
			args{
				lookup: Fields{Pair{"Friends": Fields{"Name"}}},
				tree:   alfan,
			},
			struct {
				Friends []struct {
					Name string
				}
			}{[]struct{ Name string }{{"Fuja"}, {"Hadi"}}},
		},
		{
			"Using interface{} for Lookup",
			args{
				lookup: []interface{}{"Name", "Age"},
				tree:   fuja,
			},
			struct {
				Name string
				Age  int
			}{"Fuja", 21},
		},
		{
			"Using Pair to lookup empty slice",
			args{
				lookup: Fields{Pair{"Friends": Fields{"Name"}}},
				tree:   fuja,
			},
			struct {
				Friends []struct {
					Name string
				}
			}{[]struct{ Name string }{}},
		},
		{
			"Using invalid Pair",
			args{
				lookup: []interface{}{map[interface{}]interface{}{"Friends": "Name"}},
				tree:   fuja,
			},
			struct {
				Friends []struct{}
			}{[]struct{}{}},
		},
		{
			"Using lookup slice of struct",
			args{
				lookup: []interface{}{"Name"},
				tree:   peoples,
			},
			[]struct {
				Name string
			}{
				{"Alfan"},
				{"Hadi"},
				{"Fuja"},
			},
		},
		{
			"Using lookup slice of struct with non-string key",
			args{
				lookup: []interface{}{1},
				tree:   peoples,
			},
			[]struct{}{
				{},
				{},
				{},
			},
		},
		{
			"lookup map key found",
			args{
				lookup: []interface{}{"kind"},
				tree:   fuja.Pets[0],
			},
			map[string]interface{}{"kind": "cat"},
		},
		{
			"lookup map key not found",
			args{
				lookup: []interface{}{"name"},
				tree:   fuja.Pets[0],
			},
			map[string]interface{}{},
		},
		{
			"lookup slice of map ",
			args{
				lookup: []interface{}{"kind"},
				tree:   fuja.Pets,
			},
			[]map[string]interface{}{{"kind": "cat"}, {"kind": "mouse"}},
		},
		{
			"lookup slice of map ",
			args{
				lookup: []interface{}{"kind"},
				tree:   fuja.Pets,
			},
			[]map[string]interface{}{{"kind": "cat"}, {"kind": "mouse"}},
		},
		{
			"lookup slice of string",
			args{
				lookup: []interface{}{map[interface{}]interface{}{"Hobbies": 0}},
				tree:   fuja.Pets,
			},
			[]map[string]interface{}{{}, {}},
		},
		{
			"lookup all tree-like node",
			args{
				lookup: []interface{}{"Name", "Todos", "Pets", "YearlyActivities"},
				tree:   peoples,
			},
			[]struct {
				Name             string
				Todos            map[string]string
				Pets             []map[string]interface{}
				YearlyActivities map[int]map[int]string
			}{
				{
					Name: "Alfan",
					Todos: map[string]string{
						"eat":   "done",
						"sleep": "done",
						"poop":  "on progress",
					},
				},
				{
					Name: "Hadi",
					YearlyActivities: map[int]map[int]string{
						2019: {11: "Back to Bandung"},
						2010: {
							03: "Resign from Z*****",
							04: "Start a new life",
						},
					},
				},
				{
					Name: "Fuja",
					Pets: []map[string]interface{}{
						{
							"kind":  "cat",
							"total": 2,
							"petMenusFeed": PetMenusFeed{
								Morning:   "fried fish",
								Afternoon: "fried chicken",
								Night:     "beef burger",
							},
							"indentities": []PetIdentity{
								{
									Name:   "Gar",
									Origin: "unknown",
								},
								{
									Name:   "Field",
									Origin: "unknown",
									Age:    10,
								},
							},
						},
						{
							"kind":  "mouse",
							"total": 5,
						},
					},
				},
			},
		},
		{
			"deep lookup",
			args{
				lookup: Fields{
					Pair{
						"Pets": Fields{
							"kind",
							Pair{
								"petMenusFeed": Fields{
									"Morning",
									"Night",
								},
							},
						},
					},
				},
				tree: fuja,
			},
			struct {
				Pets []map[string]interface{}
			}(struct {
				Pets []map[string]interface{}
			}{
				Pets: []map[string]interface{}{
					{
						"kind": "cat",
						"petMenusFeed": struct {
							Morning string
							Night   string
						}{"fried fish", "beef burger"},
					},
					{"kind": "mouse"},
				},
			},
			),
		},
		{
			"deep lookup with slice of struct inside map",
			args{
				lookup: Fields{
					Pair{
						"Pets": Fields{
							"kind",
							Pair{"indentities": Fields{"Name"}},
						},
					},
				},
				tree: fuja,
			},
			struct {
				Pets []map[string]interface{}
			}{
				Pets: []map[string]interface{}{
					{
						"indentities": []struct {
							Name string
						}{
							{Name: "Gar"},
							{Name: "Field"},
						},
						"kind": "cat",
					},
					{"kind": "mouse"}}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := Lookup(tt.args.lookup, tt.args.tree)
			assert.Equal(t, tt.want, got, tt.name)
		})
	}
}
