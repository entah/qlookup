# Rationale

I Need a tool to minimize response from endpoint and provide a minimal scope I concerned from the data.

This package inspired by [fulcrologic/fulcro](https://github.com/fulcrologic/fulcro) component queries and
[Datomic](https://docs.datomic.com/cloud/query/query-data-reference.html) pull pattern.



# Example

  consider you have a struct like this as a response from an endpoint:
  ``` go
  type Person struct {
		Name             string
		Age              int
		Friends          []Person
		Pets             []map[string]interface{}
		Hobbies          []string
		Todos            map[string]string
		YearlyActivities map[int]map[int]string
	}
  ```

  And you only need to look for some value like `Name` and `Name` field of a `Person`.
  You can create a lookup path like:

  JSON representation:

  ``` javascript
  [
    "Name",
    {
      "Friends":
      [
        "Name"
      ]
    }
  ]
  ```

  Array of interface{} representation:

  ``` go
   []interface{}{
        "Name",
        map[string][]string{
            "Friends": {"Name"},
        },
   }
  ```

  Go type representation:

  ``` go
   Fields{
        "Name",
        Pair{
           "Friends": Fields{"Name"},
        },
   }
  ```

  then you will get a struct like:

  ``` go
  struct{
     Name     string
     Friends []struct{
          Name  string
     }
  }
  ```

  The bonus is all the struct field's struct is preserved.

  Try in [playground](https://play.golang.org/p/LZXBHQXDXN5)
