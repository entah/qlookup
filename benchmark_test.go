package qlookup

import "testing"

func BenchmarkLookup(b *testing.B) {
	type args struct {
		lookup interface{}
		tree   interface{}
	}
	tests := []struct {
		name string
		args args
		want interface{}
	}{
		{
			"Deep lookup slice of struct inside map",
			args{
				lookup: Fields{
					Pair{
						"Pets": Fields{
							"kind",
							Pair{"indentities": Fields{"Name"}},
						},
					},
				},
				tree: fuja,
			},
			struct {
				Pets []map[string]interface{}
			}{
				Pets: []map[string]interface{}{
					{
						"indentities": []struct {
							Name string
						}{
							{Name: "Gar"},
							{Name: "Field"},
						},
						"kind": "cat",
					},
					{"kind": "mouse"}}},
		},
		// {
		// 	"Key Found",
		// 	args{
		// 		lookup: Fields{"Name"},
		// 		tree:   alfan,
		// 	},
		// 	struct{ Name string }{"Alfan"},
		// },
		// {
		// 	"Key Not Found",
		// 	args{
		// 		lookup: Fields{"Parents"},
		// 		tree:   alfan,
		// 	},
		// 	struct{}{},
		// },
		// {
		// 	"Pair Elem",
		// 	args{
		// 		lookup: Fields{Pair{"Friends": Fields{"Name"}}},
		// 		tree:   alfan,
		// 	},
		// 	struct {
		// 		Friends []struct {
		// 			Name string
		// 		}
		// 	}{[]struct{ Name string }{{"Fuja"}, {"Hadi"}}},
		// },
		// {
		// 	"Interface{} Lookup",
		// 	args{
		// 		lookup: []interface{}{"Name", "Age"},
		// 		tree:   fuja,
		// 	},
		// 	struct {
		// 		Name string
		// 		Age  int
		// 	}{"Fuja", 21},
		// },
		// {
		// 	"Pair lookup empty slice",
		// 	args{
		// 		lookup: Fields{Pair{"Friends": Fields{"Name"}}},
		// 		tree:   fuja,
		// 	},
		// 	struct {
		// 		Friends []struct {
		// 			Name string
		// 		}
		// 	}{[]struct{ Name string }{}},
		// },
		// {
		// 	"Invalid Pair",
		// 	args{
		// 		lookup: []interface{}{map[interface{}]interface{}{"Friends": "Name"}},
		// 		tree:   fuja,
		// 	},
		// 	struct {
		// 		Friends []struct{}
		// 	}{[]struct{}{}},
		// },
		// {
		// 	"Slice of struct",
		// 	args{
		// 		lookup: []interface{}{"Name"},
		// 		tree:   peoples,
		// 	},
		// 	[]struct {
		// 		Name string
		// 	}{
		// 		{"Alfan"},
		// 		{"Hadi"},
		// 		{"Fuja"},
		// 	},
		// },
		// {
		// 	"Slice of struct non-string key",
		// 	args{
		// 		lookup: []interface{}{1},
		// 		tree:   peoples,
		// 	},
		// 	[]struct{}{
		// 		{},
		// 		{},
		// 		{},
		// 	},
		// },
		// {
		// 	"Map key found",
		// 	args{
		// 		lookup: []interface{}{"kind"},
		// 		tree:   fuja.Pets[0],
		// 	},
		// 	map[string]interface{}{"kind": "cat"},
		// },
		// {
		// 	"Map key not found",
		// 	args{
		// 		lookup: []interface{}{"name"},
		// 		tree:   fuja.Pets[0],
		// 	},
		// 	map[string]interface{}{},
		// },
		// {
		// 	"Slice of map ",
		// 	args{
		// 		lookup: []interface{}{"kind"},
		// 		tree:   fuja.Pets,
		// 	},
		// 	[]map[string]interface{}{{"kind": "cat"}, {"kind": "mouse"}},
		// },
		// {
		// 	"Slice of string",
		// 	args{
		// 		lookup: []interface{}{map[interface{}]interface{}{"Hobbies": 0}},
		// 		tree:   fuja.Pets,
		// 	},
		// 	[]map[string]interface{}{{}, {}},
		// },
		// {
		// 	"Tree-like node",
		// 	args{
		// 		lookup: []interface{}{"Name", "Todos", "Pets", "YearlyActivities"},
		// 		tree:   peoples,
		// 	},
		// 	[]struct {
		// 		Name             string
		// 		Todos            map[string]string
		// 		Pets             []map[string]interface{}
		// 		YearlyActivities map[int]map[int]string
		// 	}{
		// 		{
		// 			Name: "Alfan",
		// 			Todos: map[string]string{
		// 				"eat":   "done",
		// 				"sleep": "done",
		// 				"poop":  "on progress",
		// 			},
		// 		},
		// 		{
		// 			Name: "Hadi",
		// 			YearlyActivities: map[int]map[int]string{
		// 				2019: {11: "Back to Bandung"},
		// 				2010: {
		// 					03: "Resign from Z*****",
		// 					04: "Start a new life",
		// 				},
		// 			},
		// 		},
		// 		{
		// 			Name: "Fuja",
		// 			Pets: []map[string]interface{}{
		// 				{
		// 					"kind":  "cat",
		// 					"total": 2,
		// 					"petMenusFeed": PetMenusFeed{
		// 						Morning:   "fried fish",
		// 						Afternoon: "fried chicken",
		// 						Night:     "beef burger",
		// 					},
		// 					"indentities": []PetIdentity{
		// 						{
		// 							Name:   "Gar",
		// 							Origin: "unknown",
		// 						},
		// 						{
		// 							Name:   "Field",
		// 							Origin: "unknown",
		// 							Age:    10,
		// 						},
		// 					},
		// 				},
		// 				{
		// 					"kind":  "mouse",
		// 					"total": 5,
		// 				},
		// 			},
		// 		},
		// 	},
		// },
		// {
		// 	"Deep lookup",
		// 	args{
		// 		lookup: Fields{
		// 			Pair{
		// 				"Pets": Fields{
		// 					"kind",
		// 					Pair{
		// 						"petMenusFeed": Fields{
		// 							"Morning",
		// 							"Night",
		// 						},
		// 					},
		// 				},
		// 			},
		// 		},
		// 		tree: fuja,
		// 	},
		// 	struct {
		// 		Pets []map[string]interface{}
		// 	}(struct {
		// 		Pets []map[string]interface{}
		// 	}{
		// 		Pets: []map[string]interface{}{
		// 			{
		// 				"kind": "cat",
		// 				"petMenusFeed": struct {
		// 					Morning string
		// 					Night   string
		// 				}{"fried fish", "beef burger"},
		// 			},
		// 			{"kind": "mouse"},
		// 		},
		// 	},
		// 	),
		// },
	}

	for _, tt := range tests {
		// b.ResetTimer()
		b.Run(tt.name, func(bb *testing.B) {
			for i := 0; i < bb.N; i++ {
				Lookup(tt.args.lookup, tt.args.tree)
			}
		})
	}
}
