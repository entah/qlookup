// Copyright (c) 2021 Ragil Rynaldo Meyer. All rights reserved.

// Package qlookup is tool for looking up a data like-tree and create a new structure for every match key.
package qlookup

import (
	"reflect"
)

type (
	// Elem pointing to field
	Elem interface{}
	// Fields is list of `gitlab.com/entah/qlookup#Elem`.
	Fields []Elem
	// Pair is keyed elem to join the tree.
	Pair map[Elem]Fields

	// StructKeyLookupFn func for customizing struct field search.
	StructKeyLookupFn func(key string, rtype reflect.Type) (reflect.StructField, bool)
)

// Lookup a data like tree, create a new structure for every match elem.
/*
Example:
  consider you have a struct like this as a response from an endpoint:
  `
  type Person struct {
		Name             string
		Age              int
		Friends          []Person
		Pets             []map[string]interface{}
		Hobbies          []string
		Todos            map[string]string
		YearlyActivities map[int]map[int]string
	}
  `

  And you only need to look for some value like `Name` and `Name` field of a `Person`.
  You can create a lookup path like:

  JSON representation:

  `
  [
    "Name",
    {
      "Friends":
      [
        "Name"
      ]
    }
  ]
  `

  Array of interface{} representation:

  `
   []interface{}{
        "Name",
        map[string][]string{
            "Friends": {"Name"},
        },
   }
  `

  Go type representation:

  `
   Fields{
        "Name",
        Pair{
           "Friends": Fields{"Name"},
        },
   }
  `

  then you will get a struct like:

  `
  struct{
     Name     string
     Friends []struct{
          Name  string
     }
  }
  `

  The bonus is all the struct field's struct is preserved.

*/
func Lookup(pathLookup interface{}, src interface{}) interface{} {
	return LookupWithSKLFn(pathLookup, src, DefaultStructKeyLookupFn)
}

// LookupWithSKLFn like `gitlab.com/entah/qlookup#Lookup` but using custom `gitlab.com/entah/qlookup#StructKeyLookupFn`.
func LookupWithSKLFn(pathLookup interface{}, src interface{}, skl StructKeyLookupFn) interface{} {
	rvLookup := reflect.ValueOf(pathLookup)
	rvTree := reflect.ValueOf(src)
	qrv := ensureNotInterface(rvLookup)
	srcrv := ensureNotInterface(rvTree)

	switch {
	case srcrv.Type().Kind() == reflect.Slice:
		return iterateSlice(qrv, srcrv, skl).Interface()
	case srcrv.Type().Kind() == reflect.Map:
		return fromFieldsMap(qrv, srcrv, skl).Interface()
	default:
		return fromFields(qrv, srcrv, skl).Interface()
	}
}

// DefaultStructKeyLookupFn default of `gitlab.com/entah/qlookup#StructKeyLookupFn` which is looking only by the field name.
func DefaultStructKeyLookupFn(key string, rtype reflect.Type) (reflect.StructField, bool) {
	return rtype.FieldByName(key)
}

func fromFields(pathLookup, tree reflect.Value, skl StructKeyLookupFn) reflect.Value {
	fields := []reflect.StructField{}
	fieldsValue := []reflect.Value{}

	if pathLookup.Type().Kind() == reflect.Slice {
		sliceLen := pathLookup.Len()
		for i := 0; i < sliceLen; i++ {
			fieldKey := ensureNotInterface(pathLookup.Index(i))

			switch {

			case fieldKey.IsValid() && fieldKey.Type().Kind() == reflect.String:
				key := fieldKey.String()

				if fieldType, ok := skl(key, tree.Type()); ok {
					fields = append(fields, fieldType)
					fieldsValue = append(fieldsValue, tree.FieldByIndex(fieldType.Index))
				}

			case fieldKey.IsValid() && fieldKey.Type().Kind() == reflect.Map:
				fieldType, fieldValue := fromPair(fieldKey, tree, skl)
				if fieldValue.IsValid() {
					fields = append(fields, fieldType)
					fieldsValue = append(fieldsValue, fieldValue)
				}
			}
		}
	}

	fieldLen := len(fields)
	structType := reflect.StructOf(fields)
	newStruct := reflect.New(structType)
	for i := 0; i < fieldLen; i++ {
		newStruct.Elem().Field(i).Set(fieldsValue[i])
	}
	return reflect.Indirect(newStruct)
}

func fromPair(pathLookup, tree reflect.Value, skl StructKeyLookupFn) (reflect.StructField, reflect.Value) {
	var resFieldType reflect.StructField
	var resFieldValue reflect.Value
	for _, selectedKey := range pathLookup.MapKeys() {

		if keyrv := ensureNotInterface(selectedKey); keyrv.IsValid() {

			if keyrv.Type().Kind() == reflect.String {
				key := keyrv.String()

				if fieldType, ok := skl(key, tree.Type()); ok {
					val := ensureNotInterface(pathLookup.MapIndex(keyrv))
					srcVal := tree.FieldByIndex(fieldType.Index)

					if srcVal.Type().Kind() == reflect.Struct {
						res := fromFields(val, srcVal, skl)
						fieldType.Type = res.Type()
						resFieldType = fieldType
						resFieldValue = res
						break
					}

					res := iterateSlice(val, srcVal, skl)
					fieldType.Type = res.Type()
					resFieldType = fieldType
					resFieldValue = res
					break
				}
			}
		}
	}
	return resFieldType, resFieldValue
}

func fromPairMap(pathLookup, tree reflect.Value, skl StructKeyLookupFn) (reflect.Value, reflect.Value) {
	var key, val reflect.Value
	for _, selectedKey := range pathLookup.MapKeys() {
		if keyrv := ensureNotInterface(selectedKey); keyrv.IsValid() {

			if mapVal := tree.MapIndex(keyrv); mapVal.IsValid() {
				currentTree := ensureNotInterface(mapVal)
				currentLookupPath := pathLookup.MapIndex(keyrv)

				if currentTree.Type().Kind() == reflect.Struct {
					val = fromFields(currentLookupPath, currentTree, skl)
					key = keyrv
					break
				}

				val = iterateSlice(currentLookupPath, currentTree, skl)
				key = keyrv
				break
			}
		}
	}
	return key, val
}

func fromFieldsMap(pathLookup, tree reflect.Value, skl StructKeyLookupFn) reflect.Value {
	mapType := reflect.TypeOf(tree.Interface())
	newMap := reflect.MakeMap(mapType)

	pathLookupLen := pathLookup.Len()
	for i := 0; i < pathLookupLen; i++ {

		if fieldKey := ensureNotInterface(pathLookup.Index(i)); fieldKey.IsValid() {

			if tree.Type().Kind() == reflect.Map {
				if fieldKey.Type().Kind() == reflect.Map {
					key, val := fromPairMap(fieldKey, tree, skl)
					if val.IsValid() {
						newMap.SetMapIndex(key, val)
					}
					continue
				}
				val := tree.MapIndex(fieldKey)
				newMap.SetMapIndex(fieldKey, val)
				continue
			}
		}
	}
	return newMap
}

func iterateSlice(pathLookup, slice reflect.Value, skl StructKeyLookupFn) reflect.Value {
	srcLen := slice.Len()

	if srcLen < 1 {
		zeroVal := reflect.Zero(slice.Type().Elem())
		res := fromFields(pathLookup, zeroVal, skl)
		zeroSlice := sliceOf(res)
		return zeroSlice
	}

	elems := []reflect.Value{}

	for i := 0; i < srcLen; i++ {
		tree := slice.Index(i)

		if tree.Type().Kind() == reflect.Map {
			res := fromFieldsMap(pathLookup, tree, skl)
			elems = append(elems, res)
			continue
		}

		res := fromFields(pathLookup, tree, skl)
		elems = append(elems, res)
	}

	zeroSlice := sliceOf(elems[0])
	zeroSlice = reflect.Append(zeroSlice, elems...)
	return zeroSlice
}

func sliceOf(elem reflect.Value) reflect.Value {
	sliceType := reflect.SliceOf(elem.Type())
	return reflect.MakeSlice(sliceType, 0, 0)
}

func ensureNotInterface(val reflect.Value) reflect.Value {
	if val.Type().Kind() == reflect.Interface && val.CanInterface() {
		return reflect.ValueOf(val.Interface())
	}
	return val
}
